import 'dart:io';

import 'package:flutter/material.dart';
import 'package:great_place/helpers/db_helper.dart';
import 'package:great_place/helpers/location_helper.dart';
import 'package:great_place/models/place.dart';

class Places extends ChangeNotifier {
  List<Place> _items = [];

  List<Place> get items {
    return [..._items];
  }

  Place findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }

  void addPlace(String name, File image, PlaceLocation location) async {
    final address = await LocationHelper.getPlaceAddress(location.latitude, location.longitude);
    final updatedLocation =
        PlaceLocation(latitude: location.latitude, longitude: location.longitude, address: address);
    final newPlace =
        Place(id: DateTime.now().toString(), name: name, location: updatedLocation, image: image);
    _items.add(newPlace);
    notifyListeners();
    DBHelper.insert('places', {
      'id': newPlace.id,
      'name': newPlace.name,
      'image': newPlace.image.path,
      'latitude': newPlace.location!.latitude,
      'longitude': newPlace.location!.longitude,
      'address': newPlace.location!.address!,
    });
  }

  Future<void> getPlaces() async {
    final placesList = await DBHelper.get('places');
    _items = placesList
        .map((item) => Place(
            id: item['id'],
            name: item['name'],
            location: PlaceLocation(
                latitude: item['latitude'], longitude: item['longitude'], address: item['address']),
            image: File(item['image'])))
        .toList();
    notifyListeners();
  }
}
