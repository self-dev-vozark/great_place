import 'package:flutter/material.dart';
import 'package:great_place/models/place.dart';
import 'package:great_place/providers/places.dart';
import 'package:great_place/screens/map_screen.dart';
import 'package:provider/provider.dart';

class PlacesDetailScreen extends StatelessWidget {
  static const routeName = '/place-detail';

  const PlacesDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String id = ModalRoute.of(context)!.settings.arguments as String;
    final selectedPlace = Provider.of<Places>(context, listen: false).findById(id);
    return Scaffold(
      appBar: AppBar(
        title: Text(selectedPlace.name),
      ),
      body: Column(
        children: [
          Container(
              height: 250,
              width: double.infinity,
              child: Image.file(selectedPlace.image, fit: BoxFit.cover, width: double.infinity)),
          SizedBox(height: 10),
          Text(
            selectedPlace.location!.address!,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, color: Colors.red),
          ),
          SizedBox(height: 10),
          TextButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (ctx) => MapScreen(
                        initialLocation: selectedPlace.location!,
                        isSelecting: false,
                      )));
            },
            child: Text('Open location in Maps'),
            style: TextButton.styleFrom(primary: Theme.of(context).primaryColor),
          )
        ],
      ),
    );
  }
}
