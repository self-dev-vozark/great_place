import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_place/models/place.dart';

class MapScreen extends StatefulWidget {
  final PlaceLocation initialLocation;
  final bool isSelecting;

  MapScreen(
      {this.initialLocation = const PlaceLocation(
          latitude: -7.952679,
          longitude: 112.614486,
          address: "Ketawanggede, Lowokwaru, Malang City, East Java"),
      this.isSelecting = false});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng? _pickedLocation;

  void _selectLocation(LatLng position) {
    setState(() {
      _pickedLocation = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select on Map'),
        actions: [
          if (widget.isSelecting)
            IconButton(
                onPressed: _pickedLocation == null
                    ? null
                    : () {
                        Navigator.of(context).pop(_pickedLocation);
                      },
                icon: Icon(
                  Icons.check,
                  color: Colors.white,
                ))
        ],
      ),
      body: GoogleMap(
          onTap: widget.isSelecting ? _selectLocation : null,
          markers: {
            Marker(
                markerId: MarkerId('m1'),
                position: _pickedLocation != null
                    ? _pickedLocation!
                    : LatLng(widget.initialLocation.latitude, widget.initialLocation.longitude))
          },
          initialCameraPosition: CameraPosition(
              zoom: 16,
              target: LatLng(widget.initialLocation.latitude, widget.initialLocation.longitude))),
    );
  }
}
