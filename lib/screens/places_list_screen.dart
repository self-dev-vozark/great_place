import 'package:flutter/material.dart';
import 'package:great_place/providers/places.dart';
import 'package:great_place/screens/add_place_screen.dart';
import 'package:great_place/screens/places_detail_screen.dart';
import 'package:provider/provider.dart';

class PlacesListScreen extends StatelessWidget {
  const PlacesListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Your Places"),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
                },
                icon: Icon(Icons.add))
          ],
        ),
        body: FutureBuilder(
          builder: (ctx, placesSnapshot) =>
              placesSnapshot.connectionState == ConnectionState.waiting
                  ? CircularProgressIndicator()
                  : Consumer<Places>(
                      builder: (ctx, places, child) => places.items.length <= 0
                          ? child!
                          : ListView.builder(
                              itemBuilder: (ctx, index) => ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: FileImage(places.items[index].image),
                                ),
                                title: Text(places.items[index].name),
                                subtitle: Text(places.items[index].location!.address!),
                                onTap: () {
                                  Navigator.of(context).pushNamed(PlacesDetailScreen.routeName,
                                      arguments: places.items[index].id);
                                },
                              ),
                              itemCount: places.items.length,
                            ),
                      child: Center(
                        child: Text('No places yet, please add some.'),
                      ),
                    ),
          future: Provider.of<Places>(context, listen: false).getPlaces(),
        ));
  }
}
