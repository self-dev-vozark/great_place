import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;

typedef SelectImage<T> = void Function(T a);

class ImageInput extends StatefulWidget {
  final SelectImage<File> onSelectImage;
  ImageInput({Key? key, required this.onSelectImage}) : super(key: key);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File? _storedImage;
  _takePicture() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.getImage(source: ImageSource.gallery, maxWidth: 600);
    if (imageFile != null) {
      setState(() {
        _storedImage = File(imageFile.path);
      });
      final appDirectory = await syspath.getApplicationDocumentsDirectory();
      final fileName = path.basename(imageFile.path);
      final savedImage = await _storedImage!.copy('${appDirectory.path}/$fileName');
      widget.onSelectImage(savedImage);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            width: 150,
            height: 150,
            alignment: Alignment.center,
            child: _storedImage != null
                ? Image.file(
                    _storedImage!,
                    fit: BoxFit.cover,
                    width: double.infinity,
                  )
                : Text(
                    'No image selected',
                    textAlign: TextAlign.center,
                  ),
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey),
            )),
        SizedBox(width: 10),
        Expanded(
            child: TextButton.icon(
          onPressed: _takePicture,
          icon: Icon(Icons.photo_camera),
          label: Text('select image'),
          style: TextButton.styleFrom(textStyle: TextStyle(color: Theme.of(context).primaryColor)),
        ))
      ],
    );
  }
}
