import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:great_place/helpers/location_helper.dart';
import 'package:great_place/screens/map_screen.dart';
import 'package:location/location.dart';

class LocationInput extends StatefulWidget {
  final Function(double, double) onSelectPlace;
  LocationInput(this.onSelectPlace);

  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String? _previewImageUrl;
  void _showMapPreview(double latitude, double longitude) {
    final previewUrl =
        LocationHelper.generateLocationPreviewImage(latitude: latitude, longitude: longitude);
    setState(() {
      _previewImageUrl = previewUrl;
    });
  }

  Future<void> _getCurrentLocation() async {
    final locationData = await Location().getLocation();
    _showMapPreview(locationData.latitude!, locationData.longitude!);
    widget.onSelectPlace(locationData.latitude!, locationData.longitude!);
  }

  Future<void> selectOnMap() async {
    final selectedLocation = await Navigator.of(context).push<LatLng>(MaterialPageRoute(
        fullscreenDialog: true,
        builder: (ctx) => MapScreen(
              isSelecting: true,
            )));
    if (selectedLocation == null) {
      return;
    }
    _showMapPreview(selectedLocation.latitude, selectedLocation.longitude);
    widget.onSelectPlace(selectedLocation.latitude, selectedLocation.longitude);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
          ),
          alignment: Alignment.center,
          height: 170,
          width: double.infinity,
          child: _previewImageUrl == null
              ? Text(
                  'No location chosen',
                  textAlign: TextAlign.center,
                )
              : Image.network(
                  _previewImageUrl!,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          TextButton.icon(
            onPressed: _getCurrentLocation,
            icon: Icon(Icons.location_on),
            label: Text('Current location.'),
            style:
                TextButton.styleFrom(textStyle: TextStyle(color: Theme.of(context).primaryColor)),
          ),
          TextButton.icon(
            onPressed: selectOnMap,
            icon: Icon(Icons.map),
            label: Text('Select on Map.'),
            style:
                TextButton.styleFrom(textStyle: TextStyle(color: Theme.of(context).primaryColor)),
          ),
        ])
      ],
    );
  }
}
